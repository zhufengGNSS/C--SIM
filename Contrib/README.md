This directory  contains various code which  other people have written
for the system and kindly donated to the distribution.

The software  in  this  directory  is  not  necessarily covered by the
standard Copyright. Where it is not, the copyright is given within the
files  themselves.  Contact  these people  if  you  want  any  further
information.
