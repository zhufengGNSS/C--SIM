In this directory you'll find an original technical paper on C++SIM which
was also published in the C User's Journal Vol. 12, Number 3, March 1994, as
well as the user manual for this release of C++SIM.

Note, the images directory is bitmaps that are used by the user manual, so don't move them in isolation.
